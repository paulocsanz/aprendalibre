// This file is part of aprendalibre
//
// Aprendalibre is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
"use strict"

var router = require('express').Router(),
    pages= require('./views/pages.js'),
    ajax = require('./views/ajax.js');

router.post('/login', pages.login);
router.post('/add/user', ajax.user_add);

router.get('/login', pages.login);
router.get('/admin', pages.admin);

router.get('/api/categories', ajax.get_categories);

router.get('/add/category', ajax.category_add);
router.get('/add/subject', ajax.subject_add);
router.get('/add/content', ajax.content_add);

router.get('/edit/:type/:pk', ajax.obj_edit);
router.get('/remove/:type/:pk', ajax.obj_remove);

router.get('/', pages.home);
router.get('/:category_slug/:subject_level', pages.subject);
router.get('/:category_slug/:subject_level/:content_type/:content_level', pages.content);

exports.router = router;
