// This file is part of aprendalibre
//
// Aprendalibre is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
"use strict"

var handlebars = require('handlebars'),
    path = require('path'),
    fs = require('fs'),
    Q = require('q'),
    debug = require('debug')('html_handler'),
    format = require('./format.js'),
    base_html_file = require("../../config.js").base_html_file;

handlebars.registerHelper('if_eq', function(a, b, opts) {
    if(a == b)
        return opts.fn(this);
    else
        return opts.inverse(this);
});

handlebars.registerHelper('if_n_eq', function(a, b, opts) {
    if(a != b)
        return opts.fn(this);
    else
        return opts.inverse(this);
});

handlebars.registerHelper('slugify', function(txt) {
    return format.slugify(txt);
});

handlebars.registerHelper('len', function(dict) {
    return Object.keys(dict).length;
});

handlebars.registerHelper('if_ste', function(a, b, opts) {
    if(a <= b)
        return opts.fn(this);
    else
        return opts.inverse(this);
});

handlebars.registerHelper('if_last', function(list, key, opts) {
    if (key == list.length-1)
        return opts.fn(this);
    else
        return opts.inverse(this);
});

handlebars.registerHelper('len_limit', function(string, limit) {
    return string.slice(0, limit-4) + (string.length > limit-3 ? "...":"");
});


handlebars.registerHelper('sum', function(a, b) {
    return a + b;
});

handlebars.registerHelper('get', function(dict, key) {
    return dict[key];
});

var read_html = function(_path, filename){
    var deferred = Q.defer();
    debug("Reading file: " + path.join(_path, filename));
    fs.readFile(path.join(_path, filename), 'utf-8', function(error, source){
        debug("File opened, error: " + !!error);
        if (!error) {
            deferred.resolve(source);
        } else {
            console.log("Error: " + error.message + " while reading " + filename);
            deferred.resolve('');
        }
    });
    return deferred.promise;
};

exports.send_file = function(res, _path, filename, ajax, data, callback){
    data = data || {};
    if (typeof data.url === typeof {} && typeof data.url.url === typeof "str" && data.url.redirect === true && ajax !== '') return res.redirect(data.url.url);

    debug("Send file html_handler");
    debug("Sending file: " + path.join(_path + filename));
    debug("data: " + JSON.stringify(data));

    if (ajax == ''){
        debug("ajax: true");
        read_html(_path, filename).then(function(val){
            debug("file html: " + val);
            var template = handlebars.compile(val);
            var file_html = template(data);
            debug("Compiled HTML: " + file_html);

            if (callback)
                callback(file_html, res);
            else
                res.send(file_html);
        });
    } else {
        debug("ajax: false");
        read_html(_path, filename).then(function(val){
            debug("file html: " + val);
            var template = handlebars.compile(val);
            var file_html = template(data);
            debug("Compiled HTML: " + file_html);

            read_html(_path, base_html_file).then(function(val){
                debug("base_html: " + val);
                var template = handlebars.compile(val);
                var html = template({content: file_html});
                debug("Compiled HTML: " + html);
               
                if (callback)
                    callback(html, res);
                else
                    res.send(html);
            });
        });
    }
}
