var views = require('../views/pages.js');

exports.is = function(req, res, else_url) {
    var url = (typeof res.data.url === typeof {} ? res.data.url.url.split("?")[0]:res.data.url.split("?")[0]);
    if (!req.session.auth && url !== "/login") {
        res.data.url = "/login?next=" + url;
        views.login(req, res);
    } else if (typeof else_url !== typeof undefined) {
        res.data.url = else_url;
        res.redirect(res.data.url);
    }
    return req.session.auth;
}
