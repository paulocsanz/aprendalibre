// This file is part of aprendalibre
//
// Aprendalibre is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
"use strict"

var debug_slugify = require('debug')('slugify'),
    debug_math_to_html = require('debug')('math_to_html'),
    mathjax = require('mathjax-node/lib/mj-single.js'),
    jsdom = require('jsdom').jsdom,
    Q = require('q');

exports.slugify = function(str){
    debug_slugify("slugify: " + str)
    var slug = str.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')            // Trim - from end of text
        .replace(/[áàãâ]/,'a')
        .replace(/[éèẽê]/, 'e')
        .replace(/[íìĩî]/, 'i')
        .replace(/[óòõô]/, 'o')
        .replace(/[úùũû]/, 'u')
        .replace('ç', 'c');
    debug_slugify("as slug: " + slug);
    return slug;
}

mathjax.start();

exports.math_to_html = function(html){
    debug_math_to_html("math_to_html");

    debug_math_to_html("html: " + JSON.stringify(html));
    var deferred = Q.defer();

    var doc = jsdom(html).defaultView.document;
    var errors = 0;
    var success = 0;
    var filter_tags = {'math':'MathML', 'tex':'TeX'};

    var filter_keys = Object.keys(filter_tags);
    var no_edit = 0;
    for (var x=0; x < filter_keys.length; ++x){        
        var tag = filter_keys[x]; 
        var tag_list = doc.getElementsByTagName(tag);

        debug_math_to_html("tag: " + tag + ", tag_list: " + JSON.stringify(tag_list));
        
        if (!tag_list.length) no_edit++;
        if (no_edit == filter_keys.length) break;
        for (var i=0; i<tag_list.length; ++i){
            var local_list_size = tag_list.length;
            var local_tag = tag;
            var local_obj = tag_list[i];
            debug_math_to_html("local_list_size: " + local_list_size + ", local_tag: " + local_tag + ", local_obj: " + JSON.stringify(local_obj.outerHTML));
            mathjax.typeset({
                math: local_tag == 'tex' ? local_obj.innerHTML : local_obj.outerHTML,
                format: filter_tags[local_tag],
                html: true,
            }, function(result){
                var result_div = doc.createElement("div");
                result_div.innerHTML = result.html
                debug_math_to_html("Result <" + local_tag + ">: " + JSON.stringify(result_div.innerHTML));
                debug_math_to_html("error: " + !!result.error);
                if (!result.error){
                    // Cleans up result
                    var span_list = result_div.getElementsByTagName("span");
                    var index = 0, used_span = 0;
                    debug_math_to_html("span_list: " + span_list.length);
                    while (span_list.length && span_list.length > index) {
                        debug_math_to_html("span_list[" + index + "]: " + span_list[index].outerHTML);
                        span_list[index].removeAttribute('id');
                        span_list[index].removeAttribute('class');
                        span_list[index].removeAttribute('role');
                        if (span_list[index].getAttribute('style') == '')
                            span_list[index].removeAttribute('style');
                        debug_math_to_html("span_list[" + index + "]: " + span_list[index].outerHTML);
                        if (!span_list[index].hasAttributes()) {
                            debug_math_to_html("replacing empty span tags for remove tags");
                            var remove_tag = doc.createElement("remove");
                            remove_tag.innerHTML = span_list[index].innerHTML;
                            debug_math_to_html("remove_tag.outerHTML: " + remove_tag.outerHTML);
                            debug_math_to_html("result_div.outerHTML: " + result_div.outerHTML);
                            debug_math_to_html("span_list[" + index + "].parentNode.outerHTML: " + span_list[index].parentNode.outerHTML);
                            span_list[index].parentNode.insertBefore(remove_tag, span_list[index]);
                            span_list[index].remove();
                        } else {
                            index++;
                        }
                        span_list = result_div.getElementsByTagName('span');
                        debug_math_to_html("span_list: " + span_list.length);
                    }
                    local_obj.innerHTML = result_div.innerHTML.replace(/<[//]?remove>/g, "");
                    debug_math_to_html("local_obj: " + local_obj.outerHTML);
                    success++;
                } else {
                    console.log("Mathjax error: " + result.error);
                    obj.innerHTML = "Error: " + result.error + "\n" + obj.outerHTML;
                    errors++;
                }

                if (success + errors >= local_list_size) {
                    debug_math_to_html("last span element");
                    var math_div = doc.createElement("div");
                    math_div.setAttribute("class", "math");
                    math_div.innerHTML = local_obj.innerHTML;

                    local_obj.parentNode.insertBefore(math_div, local_obj);
                    local_obj.remove();

                    debug_math_to_html("Return: " + JSON.stringify(doc.documentElement.outerHTML));
                    deferred.resolve(doc.documentElement.outerHTML);
                }
            });
        }

    }
    if (no_edit == filter_keys.length)
        deferred.resolve(html);

    return deferred.promise;
}

exports.info_to_html = function(info) {
    var deferred = Q.defer();
    exports.math_to_html(info).then(function(html) {
        html = html.split(/\n/);
        for (var i in html)
            html[i] = "[pre]" + html[i].replace(/\n/, "") + "[/pre]";
        html = html.join("").replace(/\t/g, "    ").replace(/\r/g, "");
        deferred.resolve(html);
    });
    return deferred.promise;
}

exports.content = function(data, res) {
    var content_tags = ["pre", "b", "i", "math", "tex", "tab", "h1", "h2", "a"];
    for (var i=0; i<content_tags.length;++i) {
        var t = content_tags[i];
        if (t === "tab") {
            data = data.replace("[tab]", "    ");
        } else if (t === "a") {
            var exit = false;
            while (data.indexOf("[a]") !== -1 && data.indexOf("[/a]") !== -1 && !exit) {
                var code = data.substring(data.indexOf("[a]")+3, data.indexOf("[/a]"));
                if (code.indexOf("|") === -1) {
                    exit = true;
                } else {
                    var code_list = code.split("|");
                    for (var i=0; i<code_list.length;++i) {
                        if (code_list[code_list.length-1] === "\\" && i < code_list.length-1) {
                            code_list[i] = code_list[i]+code_list[i+1];
                            delete code_list[i+1];
                            i++;
                        }
                    }
                    if (code_list.length === 2) {
                        code = code.replace("\\\\", "\\");
                        data = data.replace("[a]" + code + "[/a]", "<a href='//" + code_list[1] + "'>" + code_list[0] + "</a>");
                    }
                }
            }
        } else {
            data = data.split("[" + t + "]").join("<" + t + ">").split("[/" + t + "]").join("</" + t + ">");
        }
        data = data.split("[\\" + t + "]").join("[" + t + "]").split("[/\\" + t + "]").join("[/" + t + "]");
    }
    res.send(data);
}
