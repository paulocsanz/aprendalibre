// This file is part of aprendalibre
//
// Aprendalibre is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
"use strict"

var Sequelize = require('sequelize'),
    db_filename = require("../config.js").db_filename,
    path = require("path"),
    sequelize = new Sequelize('database', null, null, {
        dialect: 'sqlite',
        storage: path.join('..', db_filename)
    });

exports.Category = sequelize.define('Category', {
    pk: { type: Sequelize.INTEGER, unique: true, autoIncrement: true, primaryKey: true},
    name: Sequelize.STRING,
    slug: { type: Sequelize.STRING, unique: true },
}, {
    timestamps: false,
    tableName: 'category'
});

exports.Subject = sequelize.define('Subject', {
    pk: { type: Sequelize.INTEGER, unique: true, autoIncrement: true, primaryKey: true},
    name: Sequelize.STRING,
    level: Sequelize.STRING,
}, {
    timestamps: false,
    tableName: 'class_subject'
});

exports.Content = sequelize.define('Content', {
    pk: { type: Sequelize.INTEGER, unique: true, autoIncrement: true, primaryKey: true},
    name: Sequelize.STRING,
    level: Sequelize.STRING,
    info: Sequelize.STRING,
}, {
    tableName: 'content'
});

exports.DataType = sequelize.define('DataType', {
    pk: { type: Sequelize.INTEGER, unique: true, autoIncrement: true, primaryKey: true },
    slug: { type: Sequelize.STRING, unique: true },
    name: Sequelize.STRING,
}, {
    timestamps: false,
    tableName: 'data_type'
});

exports.User = sequelize.define('User', {
    pk: { type: Sequelize.INTEGER, unique: true, autoIncrement: true, primaryKey: true },
    username: Sequelize.STRING,
    password: Sequelize.STRING,
    email: Sequelize.STRING,
    salt: Sequelize.STRING
}, {
    tableName: 'user'
});

exports.Category.hasMany(exports.Subject, {as: 'subjects', foreignKey: 'category_pk'});
exports.Subject.hasMany(exports.Content, {as: 'contents', foreignKey: 'subject_pk'});
exports.Content.belongsTo(exports.DataType, {as: 'type', foreignKey: 'type_pk'});
exports.Content.belongsTo(exports.User, {as: 'user', foreignKey: 'user_pk'});

/*
exports.User.sync();
exports.Category.sync();
exports.Subject.sync();
exports.Content.sync();
exports.DataType.sync();
*/
