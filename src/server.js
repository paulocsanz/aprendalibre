// This file is part of aprendalibre
//
// Aprendalibre is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
"use strict"

var express = require('express'),
    path = require("path"),
    compression = require('compression'),
    helmet = require('helmet'),
    config = require('../config.js'),
    router = require('./router.js').router,
    sessions = require('client-sessions'),
    pages = require('./views/pages.js'),
    app = express();

app.use(compression());
app.use(express.static(path.join(__dirname, config.static_path)));
app.use(helmet());
app.use(sessions({
    cookieName: "session",
    secret: '0wi9asSAOpa93132hnszzxqwrikala991',
    duration: 1000 * 60 * 60 * 24,
    activeDuration: 1000 * 60 * 5,
}));

app.use(function(req, res, next){
    req.session.auth = !!req.session.auth;
    req.session.user = req.session.auth ? req.session.user:undefined;
    res.data = { auth: req.session.auth, url: req.url };

    if (typeof req.query.next !== typeof undefined) {
        res.data.next = req.query.next;
        res.data.url = res.data.url.replace(new RegExp("[&]?next=" + res.data.next + "/i"),"")
                                   .replace(new RegExp("[&]?ajax[=]+"),"");
        res.data.url = (res.data.url[res.data.url.length-1] == "?" ? res.data.url.splice(0, res.data.url.length-1):res.data.url);
    }
    next();
});

app.disable('x-powered-by');

app.use(router);

// Handle 404
app.use(function(req, res) {
    res.status(404);
    pages.not_found(req, res);
});
              
// Handle 500
app.use(function(error, req, res, next) {
    res.status(500);
    pages.not_found(req, res); 
});

app.listen(config.PORT, function(){
    console.log("Server running on http://localhost:" + config.PORT);
});
