// This is file part of aprendalibre
//
// Aprendalibre is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
"use strict"

var debug = require('debug')('views'),
    path = require("path"),
    qs = require('querystring'),
    api = require('./../api.js'),
    html_handler = require('./../utils/html_handler.js'),
    auth = require('./../utils/auth.js'),
    html_path = require('./../../config.js').html_path,
    full_path = path.join(__dirname, "..", html_path);

// Category
exports.get_categories = function(req, res) {
    debug("Get categories views - ajax");
    api.get_categories_slug().then(function(categories){
        debug("Categories: " + JSON.stringify(categories));
        res.send(categories);
        debug("END Get categories views - ajax");
    });
}

exports.category_add = function(req, res){
    debug("Add category - ajax");
    if (!auth.is(req, res)) return;
    debug("Authenticated");
    if (req.query.name) {
        debug("name: " + req.query.name + ", slug: " + req.query.slug);
        api.add_category(req.query.name, req.query.slug).then(function(success){
            debug("success: " + success);
            res.data.url = "./../admin";
            if (typeof req.query.json === typeof "")
                res.send(JSON.stringify({ success: success, redirect: "./../admin"}));
            else
                exports.admin(req, res);
        });
    } else {
        debug("Add category form page");
        html_handler.send_file(res, full_path, 'add_category.html', req.query.ajax, res.data);
    }
    debug("END Add category - ajax");
}

exports.category_remove = function(req, res) {
    debug("Remove category - ajax");
    if (!auth.is(req, res)) return;
    debug("Authenticated");
    api.remove_category(req.params.pk).then(function(success){
        debug("success: " + success);
        res.send({ success: success });
    });
    debug("END Remove category - ajax");
}

exports.category_edit = function(req, res) {
    debug("Edit category - ajax");
    if (!auth.is(req, res)) return;
    debug("Authenticated");
    api.edit_category(req.params.pk, req.query.name, req.query.slug).then(function(success) {
        debug("success: " + success);
        res.send({ success: success });
    });
    debug("END Edit category - ajax");
}

// Subject
exports.subject_add = function(req, res){
    debug("Add subject - ajax");
    if (!auth.is(req, res)) return;
    if (req.query.category_id && req.query.name) {
        api.add_subject(req.query.category_id, req.query.name, req.query.level || req.query.name).then(function(success){
            if (typeof req.query.json === typeof "") {
                res.send(JSON.stringify({ success: success, redirect: "./../admin" }));
            } else {
                res.data.url = "./../admin";
                exports.admin(req, res);
            }
        });
    } else {
        api.get_categories_slug().then(function(categories){
            var data = {categories: categories, category: {}};
            if (req.query.category_slug) {
                for (var i in categories) {
                    if (categories[i] === req.query.category_slug)
                        data.category = {pk: i, slug: categories[i]};
                }
            }

            for (var attrname in res.data) data[attrname] = res.data[attrname];
            html_handler.send_file(res, full_path, 'add_subject.html', req.query.ajax, data);
        });
    }
}

exports.subject_remove = function(req, res) {
    api.remove_subject(req.params.pk).then(function(success){
        res.send({ success: success });
    });
}

exports.subject_edit = function(req, res) {
    api.edit_subject(req.params.pk, req.query.name, req.query.level, req.query.category_slug).then(function(success){
        res.send({ success: success });
    });
}

// Content
exports.content_add = function(req, res){
    if (!auth.is(req, res)) return;
    var data = res.data,
        is_def = function(val) {
        return typeof val !== typeof undefind;
    };

    data.edit = typeof req.query.edit === typeof "";
    if (is_def(req.query.subject_pk) && is_def(req.query.name) && is_def(req.query.level) && is_def(req.query.type) &&
            is_def(req.query.info) && typeof req.query.create === typeof "") {

        api.add_content(req.query.subject_pk, req.query.name, req.query.level, req.query.info, req.query.type).then(function(d) {
            if (typeof req.query.json === typeof "") {
                res.send(JSON.stringify({success: d.success, redirect: d.url}));
            } else {
                res.data.url = d.url;
                api.content_params(d.pk).then(function(params){
                    req.params = params;
                    exports.content(req, res);
                });
            }
        });
    } else if (is_def(req.query.subject_pk) && is_def(req.query.name) && is_def(req.query.level) && is_def(req.query.type) && 
            is_def(req.query.pk) && is_def(req.query.info) && data.edit) {
        api.edit_content(req.query.subject_pk, req.query.name, req.query.level, req.query.info, req.query.type, req.query.pk).then(function(d) {
            if (typeof req.query.json === typeof "") {
                res.send(JSON.stringify({success: d.success, redirect: d.url}));
            } else {
                res.data.url = d.url;
                api.content_params(req.query.pk).then(function(params){
                    req.params = params;
                    exports.content(req, res);
                });
            }
        });
    } else if (req.query.type === "resumo"){
        req.query.pk = typeof req.query.edit === typeof "" ? req.query.pk : null;
        api.get_content_add(req.query.pk, req.query.subject_pk, req.query.category_slug).then(function(dict) {
            for (var attrname in dict) data[attrname] = dict[attrname];

            data.name = req.query.name || data.name;
            data.level = req.query.level || data.level;
            data.subject_level = req.query.subject_level || data.subject_level;
            data.category_slug, req.query.category_slug || data.category_slug;
            data.type_slug = req.query.type || data.type_slug;

            debug("data: " + JSON.stringify(data));
            html_handler.send_file(res, full_path, 'summary_edit.html', req.query.ajax, data);
        });
    } else {
        html_handler.send_file(res, full_path, 'summary_edit.html', req.query.ajax, data);
    }
}

exports.content_remove = function(req, res) {
    api.remove_content(req.params.pk).then(function(success){
        res.send({ success:success });
    });
}

// User
exports.user_add = function(req, res){
    if (req.method == 'POST'){
        var body = '';
        req.on('data', function(data){
            body += data;
            if (body.length > 1e6) {
                res.writeHead(413, {"Content-Type": "text/plain"}).end();
                req.connection.destroy();
            }
        });
        req.on('end', function(){
            var post = qs.parse(body);
            api.user_add(post.username, post.password, post.email).then(function(user){
                if (typeof user === typeof {}) {
                    console.log("User " + post.username + " created and authenticated");
                    req.session.auth = true;
                    req.session.user = user;
                    res.data.auth = req.session.auth;
                    res.data.url = { url: "/", redirect: true};
                    exports.home(req, res);
                } else {
                    req.method = 'GET';
                    res.data.url = { url: "/login", redirect: true }; 
                    exports.login(req, res);
                }
            });
        });
    } else {
        res.writeHead(405, {"Content-Type": "text/plain"}).end();
        res.end();
    }
}

// Generic
exports.obj_remove = function(req, res) {
    if (!auth.is(req, res)) return;
    debug("Remove " + req.params.type + " (pk=" + req.params.pk + ")");
    exports[req.params.type + "_remove"](req, res);
}

exports.obj_edit = function(req, res) {
    if (!auth.is(req, res)) return;
    debug("Edit " + req.params.type + " (pk=" + req.params.pk + ")");
    exports[req.params.type + "_edit"](req, res);
}
