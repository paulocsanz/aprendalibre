// This is file part of aprendalibre
//
// Aprendalibre is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
"use strict"

var debug = require('debug')('views'),
    path = require("path"),
    qs = require('querystring'),
    api = require('./../api.js'),
    html_handler = require('../utils/html_handler.js'),
    format = require('./../utils/format.js'),
    auth = require('./../utils/auth.js'),
    html_path = require('./../../config.js').html_path,
    full_path = path.join(__dirname, "..", html_path);

exports.not_found = function(req, res) {
    html_handler.send_file(res, full_path, '404.html', req.query.ajax, res.data);
}

// url: "/"
exports.home = function(req, res){
    debug("Home View");
    debug("ajax: " + req.query.ajax);
    var data = res.data;

    api.get_categories_show().then(function(categories){
        debug("categories: ", JSON.stringify(categories));
        data.categories = categories;
        html_handler.send_file(res, full_path, 'subjects.html', req.query.ajax, data);
        debug("Home View End\n");
    });
}

// url: "/{category_slug}/{subject_level}/"
exports.subject = function(req, res) {
    debug("Subject View");
    debug("ajax: " + req.query.ajax);
    var data = res.data;

    api.get_contents_show(req.params.category_slug, req.params.subject_level).then(function(data){
        for (var attrname in res.data) data[attrname] = res.data[attrname];
        debug("content: ", JSON.stringify(data));

        data.category.slug = req.params.category_slug;
        debug("name: " + data.name);
        debug("category.name: " + data.category.name);

        html_handler.send_file(res, full_path, 'subject.html', req.query.ajax, data);
        debug("Subject View End\n");
    });
}

// url: "/{category_slug}/{subject_level}/{content_type}/{subject_index}"
exports.content = function(req, res) {
    debug("Content View");
    debug("ajax: " + req.query.ajax);
    var category_slug = req.params.category_slug,
        subject_level = req.params.subject_level,
        content_type = req.params.content_type,
        content_level = req.params.content_level;

    debug("category_slug: " + category_slug);
    debug("subject_level: " + subject_level);
    debug("content_type: " + content_type);
    debug("content_level: " + content_level);

    api.get_content_show(category_slug, subject_level, content_type, content_level, true).then(function(data){
        for (var attrname in res.data) data[attrname] = res.data[attrname];
        debug("content: " + JSON.stringify(data.content));
        debug("content_list: " + JSON.stringify(data.content_list));

        debug("type: " + data.content.type_slug);
        if (data.content.type_slug == 'resumo')
            format.info_to_html(data.content.info).then(function(html){
                debug("formatted escaped content: " + html);
                data.content.info = html;
                html_handler.send_file(res, full_path, 'summary.html', req.query.ajax, data, format.content);
            });
        else
            exports.not_found(req, res);
    });
}

// url: "/login"
exports.login = function(req, res) {
    debug("Login view");
    if (auth.is(req, res, res.data.next)) return;
    if (req.method == 'POST') {
        debug("POST");
        var body = '';
        req.on('data', function(data){
            body += data;
            if (body.length > 1e6){
                res.writeHead(413, {"Content-Type": "text-plain"}).end();
                req.connection.destroy();
            }
        });
        req.on('end', function(){
            var post = qs.parse(body);
            debug("post: " + JSON.stringify(post));
            api.login(post.username, post.password).then(function(user) {
                if (user !== null){
                    console.log("User " + user.username + " authenticated");
                    req.session.auth = true;
                    req.session.user = user;
                    res.data.auth = req.session.auth;
                    res.data.url = { url: "/admin", redirect: true };
                    exports.admin(req, res);
                } else {
                    req.method = "GET";
                    exports.login(req, res);
                }
            });
        });
    } else {
        debug("GET");
        html_handler.send_file(res, full_path, "login.html", req.query.ajax, res.data);
    }
}

exports.admin = function(req, res) {
    if (!auth.is(req, res)) return;
    debug("Admin view");

    if (!auth.is(req, res)) return;
    debug("Username: " + req.session.user.username + ", email: " + req.session.user.email);
    api.admin_menu().then(function(data){
        for (var attrname in res.data) data[attrname] = res.data[attrname];
        debug("data: " + JSON.stringify(data));

        html_handler.send_file(res, full_path, "admin.html", req.query.ajax, data);
    });
}
