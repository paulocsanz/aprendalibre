/*
 * This file is part of aprendalibre
 *
 * Aprendalibre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>.
 */
"use strict"

window.data_types = ["category", "subject", "content"];
window.action_types = ["add"];

var category_add_submit = function(ev) {
    var inputs = this.parentNode.getElementsByTagName("input");
    ajax_request("add/category?name=" + inputs.name.value, refresh_if_success);
    ev.preventDefault();
    ev.stopPropagation();
}

var subject_add_submit = function(ev) {
    var inputs = this.parentNode.getElementsByTagName("input");
    
    ajax_request("/add/subject?category_id=" + inputs.category_id.value + "&name=" + inputs.name.value + "&level=" + inputs.level.value, refresh_if_success);
    ev.preventDefault();
    ev.stopPropagation();
}

var add_subject_ev = function(ev){
    ajax_hijack(ev, this.host, function(node) {
        show_add_box(node.className.split(" ")[0], {"input":["name", "level"]}, node.parentNode);
    }, this);
}

var add_category_ev = function(ev){
    ajax_hijack(ev, this.host, function(node) {
        show_add_box(node.className.split(" ")[0], {"input":["name"]});
    }, this);
}

document.addEventListener("DOMContentLoaded", function(event) {
    bind_other_elements();
});
