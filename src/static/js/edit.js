/*
 * This file is part of aprendalibre
 *
 * Aprendalibre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>.
 */
"use strict"

if (typeof window.data_types === typeof undefined)
    window.data_types = [];

var bind_other_elements = function(){
    var data_types = window.data_types, 
        action_types = window.action_types;
    for (var i in data_types)
        for (var x in action_types)
            bind_elements_by_tag("a", window[action_types[x] + "_" + data_types[i] + "_ev"], false, data_types[i] + " " + action_types[x]);
}

var show_add_box = function(css_class, tags, node){
    var doc = node || document;
    var div = doc.getElementsByClassName(css_class + " add hide").form;
    if (typeof div === typeof undefined) {
        div = doc.getElementsByClassName(css_class + " add").form;
        div.className = css_class + " add hide";
        if ("input" in tags)
            for (var i=0; i<tags["input"].length;++i) 
                div.getElementByTagName("input")[tags["input"][i]].value = "";
        if ("dropdown" in tags)
            for (var i=0; i<tags["dropdown"].length;++i) 
                div.getElementsByTagName("select")[tags["dropdown"][i]].selectedIndex = 0;
    } else {
        div.className = css_class + " add";
    }
    div.getElementsByTagName("input").submit.addEventListener("click", window[css_class + "_add_submit"]);
}
