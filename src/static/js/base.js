/*
 * This file is part of aprendalibre
 *
 * Aprendalibre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>.
 */
"use strict"

if (typeof window.action_types === typeof undefined)
    window.action_types = [];

var bind_elements_by_tag = function(tag, callback, bind_global=false, class_name){
    var list;
    if (bind_global)
        list = document.getElementsByTagName(tag);
    else
        list = document.getElementById("content").getElementsByTagName(tag);
    for (var i=0; i<list.length;++i){
        if (class_name && list[i].className !== class_name) continue;
        if (list[i].className !== class_name){
            var found = false;
            for (var x in window.action_types) {
                if (list[i].className.indexOf(window.action_types[x]) !== -1) {
                    found = true;
                    break;
                }
            }
            if (found) continue;
        }
        list[i].addEventListener("click", callback);
    }
}

var link_hijack_ev = function(ev){
    ajax_hijack(ev, this.host, ajax_request, this.href);
}
var ajax_hijack = function(ev, host, callback, param) {
    /* Respects ctrl+click (opens in a blank target) and only ajax indoor links */
    if (!ev.ctrlKey && !(ev.which == 2 && ev.button == 4) && host === window.location.host){
        ev.preventDefault();
        ev.stopPropagation();
        if (callback) {
            if (param)
                callback(param);
            else
                callback();
        }
    }
}

var ajax_request = function(href, content_callback){
    var xmlhttp = new XMLHttpRequest(),
        other_params = href.indexOf("?") !== -1;

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (typeof content_callback === typeof undefined) {
                if (window.history.pushState)
                    window.history.pushState({}, '', href);

                /* Prevents screen flashing by buffering the data and waiting <wait>ms (for the css) before switching content */
                var wait = 400, 
                    div = document.createElement("div"),
                    content = document.getElementById("content");

                div.id = "content";
                div.innerHTML = xmlhttp.responseText;
                div.className = "hide";
        
                content.parentNode.insertBefore(div, content);
                setTimeout(function(){
                    div.removeAttribute("class");
                    content.className = "hide";
                    content.parentNode.removeChild(content);
                }, wait);
            } else {
                content_callback(xmlhttp.responseText);
            }
            bind_elements_by_tag("a", link_hijack_ev);
            if (typeof bind_other_elements !== typeof undefined)
                bind_other_elements();
        }
    };

    xmlhttp.open("GET", href + (other_params ? "&": "?") + (typeof content_callback !== typeof undefined ? "json":"ajax"), true);
    xmlhttp.send();
}

var refresh_if_success = function(data){
    if (JSON.parse(data)["success"] === true) ajax_request(window.location.href);
    else alert("Error");
}

var ignore_if_success = function(data) {
    data = JSON.parse(data);
    var node = document.createElement("div");
    node.className = "error_message";
    node.innerHTML = "<b>Erro: " + data.message + "</b>";
    if (data["success"] === false){
        document.getElementById("document").appendChild(node);
    }
}

var update_url = function(url) {
    if (url !== window.location.href)
        window.history.replaceState({}, '', url); 
}

document.addEventListener("DOMContentLoaded", function(event) { 
    bind_elements_by_tag("a", link_hijack_ev, true);
});

/* Respects back page click (user history) */
window.addEventListener("popstate", function(event){
    ajax_request(window.location.href);
});
