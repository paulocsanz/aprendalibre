/*
 * This file is part of aprendalibre
 *
 * Aprendalibre is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http:www.gnu.org/licenses/>.
 */
"use strict"

window.data_types = ["category", "subject", "content"];
window.action_types = ["edit", "remove", "add"];

var editing = 0;
var edit_url_list = [];

var lock_element = function(el, name, type) {
    if (type === "dropdown")
        return el[name].innerHTML = el[name].getElementsByTagName("select")[0].selectedOptions[0].innerHTML;
    return el[name].innerHTML = el[name].children[name].value;
}

var edit_element = function(el, name, type, options, selected_slug) {
    if (type === "dropdown" && typeof options === typeof []) {
        var select = "<select name='category'>",
            index;
        for (var i=0;i<Object.values(options).length;++i) {
            if (typeof selected_slug !== typeof undefined && Object.values(options)[i] === selected_slug)
                index = i;
            select += "<option value='" + Object.keys(options)[i] + "'>" + Object.values(options)[i] + "</option>";
        }
        select += "</select>";
        el[name].innerHTML = select; 
        return el[name].getElementsByTagName("select")[0].selectedIndex = index;
    }
    return el[name].innerHTML = "<input name='" + name + "' value='" + el[name].innerHTML + "' />";
}

var subject_add_submit = function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    var inputs = this.parentNode.parentNode.getElementsByTagName("input"),
        category = this.parentNode.parentNode.getElementsByTagName("select").category;
    ajax_request("add/subject?category_id=" + category.value + "&name=" + inputs.name.value + "&level=" + inputs.level.value, refresh_if_success);
}

var category_add_submit = function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    var inputs = this.parentNode.parentNode.getElementsByTagName("input");
    ajax_request("add/category?name=" + inputs.name.value + "&slug=" + inputs.slug.value, refresh_if_success);
}

var content_add_submit = function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    var inputs = this.parentNode.parentNode.getElementsByTagName("input"),
        selects = this.parentNode.parentNode.getElementsByTagName("select"),
        category_slug = selects.subject[selects.subject.selectedIndex].getAttribute("data-category_slug");
    ajax_request("add/content?subject_pk=" + selects.subject.value + "&level=" + 
                    inputs.level.value + "&category_slug=" + category_slug + "&type=" + selects.type.value +
                        "&name=" + inputs.name.value);
}

var add_subject_ev = function(ev){
    ajax_hijack(ev, this.host, function(node) {
        show_add_box(node.className.split(" ")[0], {"input": ["name", "level"], "dropdown": ["category"] });
    }, this);
}

var add_category_ev = function(ev){
    if (!ev.ctrlKey && !(ev.which == 2 && ev.button == 4)){
        ev.preventDefault();
        ev.stopPropagation();
        show_add_box(this.className.split(" ")[0], {"input": ["name", "slug"] });
    }
}

var add_content_ev = function(ev) {
    if (!ev.ctrlKey && !(ev.which == 2 && ev.button == 4)){
        ev.preventDefault();
        ev.stopPropagation();
        show_add_box(this.className.split(" ")[0], {"dropdown": ["content"]});
    }
}

var edit_subject_ev = function(ev) {
    var parent_node = this.parentNode.parentNode,
        el = parent_node.children;
    if (!ev.ctrlKey && !(ev.which == 2 && ev.button == 4)){
        ev.preventDefault();
        ev.stopPropagation();
        if (parent_node.className !== "editing") {
            editing++;
            parent_node.className = "editing";
            var options = [];
            ajax_request("api/categories", function(json) {
                edit_element(el, "name");
                edit_element(el, "level");
                edit_element(el, "slug", "dropdown", JSON.parse(json), el.slug.innerText);
            });
        } else {
            parent_node.removeAttribute("class");
            var name = lock_element(el, "name"),
                level = lock_element(el, "level"),
                category_slug = lock_element(el, "slug", "dropdown");
            editing--;
            edit_url_list.push(this.getAttribute('href') + "?name=" + name + "&level=" + level + "&category_slug=" + category_slug);
            if (editing === 0) {
                for (var i in edit_url_list);
                    ajax_request(edit_url_list[i], ignore_if_success);
                edit_url_list = [];
                refresh_if_success(JSON.stringify({success: (document.getElementsByClassName("error_message").length === 0)}));
            }
        }
    }
}

var edit_category_ev = function(ev) {
    var parent_node = this.parentNode.parentNode,
        el = parent_node.children;
    if (!ev.ctrlKey && !(ev.which == 2 && ev.button == 4)){
        ev.preventDefault();
        ev.stopPropagation();
        if (parent_node.className !== "editing"){
            editing++;
            parent_node.className = "editing";
            edit_element(el, "name");
            edit_element(el, "slug");
        } else {
            parent_node.removeAttribute("class");
            var name = lock_element(el, "name"),
                slug = lock_element(el, "slug");
            editing--;
            edit_url_list.push(this.getAttribute('href') + "?name=" + name + "&slug=" + slug);
            if (editing === 0) {
                for (var i in edit_url_list);
                    ajax_request(edit_url_list[i], ignore_if_success);
                edit_url_list = [];
                refresh_if_success(JSON.stringify({success: (document.getElementsByClassName("error_message").length === 0)}));
            }

        }
    }
}

var remove_ev = function(ev) {
    if (!ev.ctrlKey && !(ev.which == 2 && ev.button == 4)){
        ev.preventDefault();
        ev.stopPropagation();
        ajax_request(this.getAttribute('href'), refresh_if_success)
    }
}

var remove_category_ev = remove_ev,
    remove_subject_ev = remove_ev,
    remove_content_ev = remove_ev;

document.addEventListener("DOMContentLoaded", function(ev) {
    bind_other_elements();
});
