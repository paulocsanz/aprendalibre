// This file is part of aprendalibre
//
// Aprendalibre is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
"use strict"

var model = require('./models.js'),
    format = require('./utils/format.js'),
    debug = require('debug')('api'),
    Q = require('q'),
    argon2 = require('argon2');

exports.get_categories_show = function(){
    // subject_obj: { name: (str), level: (str) }
    // returns: { slug (str): {
    //              id:   (str),
    //              name: (str),
    //              slug: (str),
    //              subjects: [subject_obj]}
    //          }
    return new Promise(function(resolve) {
        model.Category.findAll({
                                    include: [{model: model.Subject, as:'subjects', attributes: ["name", "level"]}],
                                    attributes: ["pk", "name", "slug"]
        }).then(function(categories){
            var ret = {};
            
            debug("get_categories_show()");
            debug("Categories: " + JSON.stringify(categories));

            for (var i in categories){
                debug("Category: " + JSON.stringify(categories[i]));
                if (!ret[categories[i].slug])
                    ret[categories[i].slug] = { pk: categories[i].pk, name: categories[i].name, slug: categories[i].slug, subjects: {} };
                for (var x in categories[i].subjects)
                    ret[categories[i].slug].subjects[categories[i].subjects[i].level] = categories[i].subjects[i];
            }
            debug("Return: " + JSON.stringify(ret));
            resolve(ret);
            debug("END get_categories_show()");
        });
    });
}

exports.get_contents_show = function(category_slug, subject_level, content_type){
    //returns: [{
    //   name: (str), 
    //   info: (str), 
    //   level: (num),
    //   type: {
    //      name: (str),
    //      slug: (str),
    //   },
    //   subject: { 
    //      pk: (num),
    //      name: (str), 
    //      level: (num),
    //      category: { 
    //          name: (str),
    //          slug: (str),
    //          level: (str),
    //      }
    //   }
    //}]
    debug("get_contents_show(" + category_slug + ", " + subject_level + ", " + content_type + ")");

    var ret_dict = {},
        where = {};
    if (category_slug)
        where.category = {slug: category_slug};
    if (subject_level)
        where.subject = {level: subject_level};
    if (content_type)
        where.type = {slug: content_type};

    return new Promise(function(resolve) {
        model.Category.findAll({
                                        where: where.category,
                                        include: [{model: model.Subject, as:'subjects', attributes: ["pk", "name", "level"], where: where.subject,
                                            include: [{model: model.Content, as:'contents', attributes: ["pk", "name", "info", "level"], where: where.content,
                                                include: [{model: model.DataType, as:'type', attributes: ["name", "slug"], where: where.type}]}]}],
                                        attributes: ["name", "slug"]
        }).then(function(categories) {
            debug("categories: " + JSON.stringify(categories));
            var content_list = [],
                subject_name,
                subject_pk,
                category_name;
            for (var i in categories) {
                var cat = categories[i]
                for (var j in cat.subjects) {
                    var sub = cat.subjects[j];
                    for (var k in sub.contents) {
                        var cont = sub.contents[k],
                            url = cat.slug + "/" + sub.level + "/" + cont.type.slug + "/" + cont.level,
                            dict = {
                                    pk: cont.pk,
                                    name: cont.name,
                                    info: cont.info,
                                    level: cont.level,
                                    type: cont.type,
                                    url: url,
                                    subject: sub
                            };
                            dict.subject.category = cat;
                        if (!subject_name) subject_name = sub.name;
                        if (!subject_pk) subject_pk = sub.pk;
                        if (!category_name) category_name = cat.name;
                        content_list.push(dict);
                        debug("content_list.push(" + JSON.stringify(dict) + ")");
                    }
                }
            }
            debug("content_list: " + JSON.stringify(content_list));

            for (var i=0; i<content_list.length;++i){
                if (!ret_dict[content_list[i].type.slug]) ret_dict[content_list[i].type.slug] = [];
                    ret_dict[content_list[i].type.slug].push(content_list[i]);
            }
            debug("Return content: " + JSON.stringify(ret_dict));
            debug("Return subject_name: " + subject_name);
            debug("Return subject_pk: " + subject_pk);
            debug("Return category_name: " + category_name);
            resolve({ name: subject_name, pk: subject_pk, category: {name: category_name}, contents: ret_dict});
            debug("END get_contents_show()");
        });
    });
}

exports.get_content_show = function(category_slug, subject_level, content_type_slug, content_level){
    debug("get_content_show(" + category_slug + ", " + subject_level + ", " + content_type_slug + ", " + content_level + ")");

    return new Promise(function(resolve) {
        exports.get_contents_show(category_slug, subject_level, content_type_slug).then(function(content_dict){
            debug("content_dict: " + JSON.stringify(content_dict));
            var content_list = content_dict.contents[content_type_slug];
            var content = { subject_name: content_dict.name, subject_pk: content_dict.pk};
            for (var i in content_list){
                delete content_list[i].info;
                delete content_list[i].type;
                asdasdas
            }
            
            debug("content_list: " + JSON.stringify(content_list));
            debug("content: " + JSON.stringify(content));
            resolve({content:content, content_list:content_list});
        });
    });
}

exports.add_subject = function(category_pk, name, level) {
    var deferred = Q.defer();
    debug("add_subject(" + category_pk + ", " + name + ", " + level + ")");
    level = format.slugify(level);
    model.Subject.create({category_id: category_pk, name: name, level: level}).then(function(){
        debug("Subject saved (" + category_pk + ", " + name + ", " + level + ")");
        deferred.resolve(true);
    });
    return deferred.promise;
}

exports.add_content = function(subject_pk, name, level, info, type_slug) {
    var deferred = Q.defer();
    level = level || format.slugify(name);
    model.DataType.find({ where: {slug: type_slug}, attributes:["pk"]}).then(function(type) {
        model.Content.create({subject_id: subject_pk, type_id:type.pk, name: name, level: level, info: info}).then(function(content){
            debug("Content saved (" + subject_pk + ", " + name + ", " + type.pk + ", " + level + ", " + info + ")");
            exports.get_content_url(subject_pk).then(function(data) {
                deferred.resolve({success: true, url: "./../" + data.category_slug + "/" + data.subject_level + "/" + type_slug + "/" + level, pk: content.pk});
            });
        });
    });
    return deferred.promise;
}

exports.edit_content = function(subject_pk, name, level, info, type_slug, pk) {
    var deferred = Q.defer(),
        content = {};
    level = level || format.slugify(name);

    model.Content.update({subject_id: subject_pk, name: name, level: level, info: info}, {where: {pk: pk}}).then(function(){
        debug("Content edited (" + subject_pk + ", " + name + ", " + level + ", " + info + ", " + pk + ")");
        exports.get_content_url(subject_pk).then(function(data) {
            deferred.resolve({success: true, url: "./../" + data.category_slug + "/" + data.subject_level + "/" + type_slug + "/" + level});
        });
    });
    return deferred.promise;
}

exports.add_category = function(name, slug) {
    var deferred = Q.defer();
    slug = slug || format.slugify(name); 
    model.Category.create({name: name, slug: slug}).then(function(){
        debug("Category saved (" + name + ", " + slug + ")");
        deferred.resolve(true);
    });
    return deferred.promise;
}

exports.user_add = function(username, password, email) {
    var deferred = Q.defer();

    argon2.generateSalt().then(salt => {
        argon2.hash(password, salt).then(hash => {
            model.User.create({username: username, password: hash, salt: salt}).then(function(){
                exports.login(username, password).then(function(user) {
                    if (user !== null)
                        deferred.resolve({username: user.username, email: user.email});
                    else
                        deferred.resolve(null);
                });
            });
        });
    });
    
    return deferred.promise;
}

exports.login = function(username, password) {
    var deferred = Q.defer();

    model.User.findOne({
                        where: {username:username},
                        raw: true,
                        attributes: ["username", "password", "salt"]
    }).then(function(user){
        if (user !== null){
            argon2.hash(password, user.salt).then(hash => {
                if (hash === user.password)
                    deferred.resolve({username: user.username, email: user.email });
                else
                    deferred.resolve(null);
            });
        } else {
            deferred.resolve(null);
        }
    });

    return deferred.promise;
}

exports.admin_menu = function(){
    var deferred = Q.defer();

    model.Category.findAll({
                            raw: true,
                            attributes: ["pk", "name", "slug"]
    }).then(function(categories){
        categories.sort(function(a, b) {
            return a.pk - b.pk;
        });
        model.Subject.findAll({
                                attributes: ["pk", "name", "level"],
                                include: [{model: model.Category, as:'category', attributes: ["name", "slug", "pk"]}],
        }).then(function(subjects_data) {
            var subjects = [];
            for (var i in subjects_data) {
                subjects_data[i].category = subjects_data[i].category || {};
                subjects[i] = {
                                pk: subjects_data[i].pk,
                                name: subjects_data[i].name,
                                level: subjects_data[i].level,
                                category: {
                                            name: subjects_data[i].category.name || "",
                                            slug: subjects_data[i].category.slug || "",
                                            pk: subjects_data[i].category.pk
                                }
                };
            }
            subjects.sort(function(a, b) {
                var no_category = {slug:0, name:"No category"};
                a.category = a.category || no_category;
                b.category = b.category || no_category;
                if (a.category.slug < b.category.slug)
                    return -1;
                else if (a.category.slug > b.category.slug)
                    return 1;
                else
                    return a.level < b.level ? -1:1;
            });
            model.Content.findAll({
                                    attributes: ["pk", "name", "level", "info"],
                                    include: [{model: model.DataType, as:'type', attributes: ["pk", "name", "slug"]},
                                              {model: model.Subject, as:'subject', attributes: ["pk", "name", "level"],
                                                  include: [{model: model.Category, as:'category', attributes: ['pk', 'name', 'slug']}]}]
            }).then(function(contents_data){
                var contents = [];
                if (contents_data === null)
                    contents = null;
                else
                    for (var i in contents_data) {
                        contents_data[i].type = contents_data[i].type || {};
                        contents_data[i].subject = contents_data[i].subject || {};
                        contents_data[i].subject.category = contents_data[i].subject.category || {}
                        contents[i] = {
                                        pk: contents_data[i].pk,
                                        name: contents_data[i].name,
                                        level: contents_data[i].level,
                                        info: contents_data[i].info,
                                        type: {
                                            pk: contents_data[i].type.pk,
                                            name: contents_data[i].type.name || "",
                                            slug: contents_data[i].type.slug || ""
                                        },
                                        subject: {
                                            pk: contents_data[i].subject.pk,
                                            name: contents_data[i].subject.name || "",
                                            level: contents_data[i].subject.level || "",
                                            category: {
                                                pk: contents_data[i].subject.category.pk,
                                                name: contents_data[i].subject.category.name || "",
                                                slug: contents_data[i].subject.category.slug || ""
                                            }
                                        }
                        }
                    }
                contents.sort(function(a, b) {
                    if (a.type && b.type && a.type.slug < b.type.slug)
                        return -1;
                    else if (a.type && b.type && a.type.slug > b.type.slug)
                        return 1;
                    else if (a.category && b.category && a.category.slug < b.category.slug)
                        return -1;
                    else if (a.category && b.category && a.category.slug > b.category.slug)
                        return 1;
                    else if (a.subject && b.subject && a.subject.slug < b.subject.slug)
                        return -1;
                    else if (a.subject && b.subject &&a.subject.slug > b.subject.slug)
                        return 1;
                    else
                        return (a.level || 0) < (b.level || 0) ? -1:1;
                });
                model.DataType.findAll({ raw: true, attributes: ["name", "slug"]}).then(function(types) {
                    var types_dict = {}
                    for (var i in types) types_dict[types[i].slug] = types[i].name;
                    deferred.resolve({ categories: categories, subjects: subjects, contents: contents, types: types_dict });
                });
            });
        });
    });

    return deferred.promise;
}

exports.remove_category = function(pk){
    var deferred = Q.defer();
    debug("Remove category API (pk = " + pk + ")");
    model.Subject.update({category_id: null}, {where: {category_id: pk}}).then(function(args){
        debug("Subjects updated: " + args[0]);
        model.Category.destroy({where: { pk: pk }}).then(function(num) {
            debug("Categories updated: " + num);
            deferred.resolve(num > 0);
        });
    });
    return deferred.promise;
}

exports.edit_category = function(pk, name, slug) {
    var deferred = Q.defer();
    debug("Edit category API (pk = " + pk + ", name = " + name + ", slug = " + slug + ")");
    model.Category.update({name:name, slug:slug}, {where: {pk: pk}}).then(function(args){
        debug("Categories updated: " + args[0]);
        deferred.resolve(args[0] > 0);
    });
    return deferred.promise;
}

exports.remove_subject = function(pk) {
    var deferred = Q.defer();
    debug("Remove subject API (pk = " + pk + ")");
    model.Subject.destroy({where: {pk: pk}}).then(function(num){
        debug("Subject removed (" + num +")");
        deferred.resolve(num>0);
    });
    return deferred.promise;
}

exports.remove_content = function(pk) {
    var deferred = Q.defer();
    debug("Remove content API (pk = " + pk + ")");
    model.Content.destroy({where: {pk:pk}}).then(function(num){
        debug("Content removed (" + num + ")");
        deferred.resolve(num>0);
    });
    return deferred.promise;
}

exports.edit_subject = function(pk, name, level, category_slug) {
    var deferred = Q.defer();
    debug("Edit subject API (pk = " + pk + ", name = " + name + ", level = " + level + ", category_slug = " + category_slug + ")");
    model.Category.find({where: {slug:category_slug}}).then(function(category){
        var cat_pk = category.pk;
        model.Subject.update({name:name, level:level, category_id:cat_pk}, {where: {pk: pk}}).then(function(args){
            debug("Subjects updated: " + args[0]);
            deferred.resolve(args[0]>0);
        });
    });
    return deferred.promise;
}

exports.get_categories_slug = function(){
    var deferred = Q.defer();
    debug("Get categories slug API");
    model.Category.findAll().then(function(categories){
        var cat_slug_by_pk = {};
        for (var i in categories)
            cat_slug_by_pk[categories[i].pk] = categories[i].slug;
        deferred.resolve(cat_slug_by_pk);
    });
    return deferred.promise;
}

exports.get_subjects = function() { 
    var deferred = Q.defer();
    model.Subject.findAll({ raw: true,
                            attributes: ["pk", "name", "level"],
                            include: [{model: model.Category, as:'category', attributes: ["slug"]}]
    }).then(function(subjects) {
        deferred.resolve(subjects);
    });
    return deferred.promise;
}

exports.get_content = function(pk) {
    debug("Get content API");
    var deferred = Q.defer(),
        where = {};

    if (pk === null)
        deferred.resolve(null);
    model.Content.findOne({where: {pk: pk},
                           include: [{model: model.DataType, as:'type', attributes: ["slug"]},
                                     {model: model.Subject, as:'subject', attributes: ["pk", "level"],
                                         include: {model: model.Category, as:'category', attributes: ["slug"]}}]
    }).then(function(content) {
        debug("content: " + JSON.stringify(content));
        var data = {};
        if (content !== null) {
            data.pk = content.pk;
            data.name = content.name;
            data.level = content.level;
            data.info = content.info;
            data.type_slug = content.type.slug;
            data.type_pk = content.type.pk;
            data.category_slug = content.subject.category.slug;
            data.subject_level = content.subject.level;
            data.subject_pk = content.subject.pk;
        } else {
            data = null
        }

        debug("data: " + JSON.stringify(data));
        debug("Get content API END");
        deferred.resolve(data);
    });
    return deferred.promise;
}

exports.get_content_add = function(pk, category_slug) {
    var deferred = Q.defer(),
        data = {};
    debug("Get content add API");
    exports.get_content(pk).then(function(content) {
        data = content || {};
        data.category_slug = category_slug || data.category_slug;

        exports.get_subjects(data.category_slug).then(function(subjects) {
            data.subjects = {};
            for (var i in subjects)
                data.subjects[subjects[i].pk] = {name: subjects[i].name, category_slug: subjects[i]["category.slug"]};

            debug("data: " + JSON.stringify(data));
            debug("Get content add API END");
            deferred.resolve(data); 
        });
    });
    return deferred.promise;
}

exports.get_content_url = function(subject_pk) {
    var deferred = Q.defer();
    model.Subject.find({where: {pk: subject_pk}, attributes: ["level"],
                        include: { model: model.Category, as:'category', attributes: ["slug"]} 
    }).then(function(subject) {
        if (subject === null)
            subject = {category:{}};
        deferred.resolve({category_slug: subject.category.slug, subject_level: subject.level});
    });
    return deferred.promise;
}

exports.content_params = function(content_pk) {
    var deferred = Q.defer();
    model.Content.find({where: {pk: content_pk}, attributes: ["level"], 
                            include: [ 
                                { model: model.DataType, as:'type', attributes: ["slug"]},
                                { model: model.Subject, as:'subject', attributes: ["level"],
                                    include: { model: model.Category, as:'category', attributes: ["slug"]}}]
    }).then(function(content) {
        var data = {};

        data.category_slug = content.subject.category.slug;
        data.subject_level = content.subject.level;
        data.content_type = content.type.slug;
        data.content_level = content.level;
        deferred.resolve(data);
    });
    return deferred.promise;
}
