"use strict"

exports.DEBUG = true;

exports.PORT = 3000;

exports.base_html_file = "base.html";
exports.html_path = "html";

exports.static_path_debug = "static";
exports.static_path_release = "static-min";

if (exports.DEBUG) 
    exports.static_path = exports.static_path_debug;
else
    exports.static_path = exports.static_path_release;

exports.db_filename = "db.db3";
