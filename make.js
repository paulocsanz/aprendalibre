"use strict"
var fs = require('fs'),
    path = require('path'),
    config = require("./config.js"),
    model = require("./src/models.js");


// Create db and save it to ./db.db3
console.log("db- Creating db")
fs.stat(path.join(__dirname, config.db_filename), function(err, exists){
    if ((err ? err.code : {}) != "ENOENT") console.log("db- " + config.db_filename + " file already exists");
    console.log("db- making User table");
    model.User.sync().then(function(){
        console.log("db- making Category table");
        model.Category.sync().then(function(){
            console.log("db- making DataType table");
            model.DataType.sync().then(function(){
                console.log("db- making Subject table");
                model.Subject.sync().then(function(){
                    console.log("db- making Content table");
                    model.Content.sync().then(function(){
                        console.log("db- saving db to ./" + config.db_filename)
                        fs.rename(path.join(__dirname, "..", config.db_filename), path.join(__dirname, config.db_filename), function(error){
                            if (error)
                                console.log(error);
                            console.log("db- saved db to ./" + config.db_filename)
                        });
                    });
                });
            });
        });
    });
});

// Checks if release static folder exists and create subfolders
if (!fs.existsSync(path.join(__dirname, "src", config.static_path_release))){
    console.log("mkdir- Making release dir: " + config.static_path_release);
    fs.mkdirSync(path.join(__dirname, "src", config.static_path_release));

    var sub_folders = ["js", "css", "fonts"];
    for (var i in sub_folders){
        console.log("mkdir- making release " + sub_folders[i] + " folder: " + config.static_path_release + "/" + sub_folders[i]);
        fs.mkdirSync(path.join(__dirname, "src", config.static_path_release, sub_folders[i]));
    }
}

// Run uglifyjs and save minified javascript to release static location
console.log("js- Uglifying js and saving to ./src/" + config.static_path_release);
var uglifyjs = require("uglify-js");

fs.readdir(path.join(__dirname, "src", config.static_path_debug, "js"), function(err, list) {
    if (err) return console.log(JSON.stringify(error));
    console.log("js- reading js static dir, number of files to uglify: " + list.length);

    list.forEach(function(file){
        console.log("js- uglifying js file: " + file);
        var normal_js_path = path.join(__dirname, "src", config.static_path_debug, "js", file),
            minified = uglifyjs.minify(normal_js_path, {
                mangle: {
                    toplevel: true,
                },
                compress: {
                    sequences: true,
                    dead_code: true,
                    drop_debugger: true,
                    conditionals: true,
                    evaluate: true,
                    booleans: true,
                    loops: true,
                    unused: true,
                    if_return: true,
                    join_vars: true,
                    warnings: true,
                    drop_console: true,
                }
            }).code;
        
        console.log("js- saving uglifyied file to " + config.static_path_release);

        fs.writeFile(path.join(__dirname, "src", config.static_path_release, "js", file), minified, function(err) {
            if (err) return console.log("ujs- " + JSON.stringify(err));
            console.log("js- saved uglifyied file to " + config.static_path_release + "/js/" + file);
        });
    });
});

// Copy all the uncompiled files in the debug static folder to the release static folder
var folders_to_copy = ["fonts", "img"];
for (var x in folders_to_copy){
    var name = folders_to_copy[x];
    console.log(name + "- Copy " + name + " to release folder");
    fs.readdir(path.join(__dirname, "src", config.static_path_debug, name), function(err, list){
        if (err) return console.log(JSON.stringify(error));
        console.log(name + "- reading " + name + " static dir, number of files to move: " + list.length);

        list.forEach(function(file){
            console.log(name + "- copying " + name + " file: " + file);
            fs.readFile(path.join(__dirname, "src", config.static_path_debug, name, file), "utf-8", function(err, source){
                if (err) return console.log(JSON.stringify(err));
                fs.writeFile(path.join(__dirname, "src", config.static_path_release, name, file), source, function(err){
                    if (err) return console.log(JSON.stringify(err));
                    console.log(name + "- copied " + name + " file: " + file);
                });
            });
        });
    });
}

// Uglify css and save to release static folder
console.log("css- Uglify css and save to release static folder");
var cleancss = require("clean-css");
fs.readdir(path.join(__dirname, "src", config.static_path_debug, "css"), function(err, list){
    if (err) return console.log(JSON.stringify(err));
    console.log("css- reading css files from static dir, number of files to uglify: " + list.length);

    list.forEach(function(file){
        console.log("css- uglifying css file: " + file);
        fs.readFile(path.join(__dirname, "src", config.static_path_debug, "css", file), "utf-8", function(err, source){
            if (err) return console.log(JSON.stringify(err));
            var minified = new cleancss().minify(source).styles;
            fs.writeFile(path.join(__dirname, "src", config.static_path_release, "css", file), minified, function(err){
                if (err) return console.log(JSON.stringify(err));
            });
        });
    });
});
