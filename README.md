#aprendalibre - let knowledge be free

Web-app to share content in an organized way.

# Installing and running

*Needs* ***node*** and ***npm***

Run

`npm install`

`node src/server.js`
